#! /bin/bash

# CORE DIR - you need an images sub-directory (everything you've used for colmap), model-txt sub-directory
# which is the exported txt files from colmap model converter command, then its up to you if you want a different
# OUTPUT_DIR sub-directory for the nerfstudio transforms 
core_dir=/vol/research/K9/people-centred-sim/13_sept_run 

COLMAP_DIR="${core_dir}/model-txt"
IMAGE_DIR="${core_dir}/images"
OUTPUT_DIR="${core_dir}" # could add sub-directory
CODE_PATH="/vol/research/K9/people-centred-sim/pipeline" # change to whereever you pull the code to
EXP_NAME="EXP_X" # makes it easier to find the checkpoint - otherwise goes under a timestamp 
MODEL="nerfacto"

echo 'Generate Transforms'
python3 $CODE_PATH/create_nerfstudio_train.py --colmap_dir "${COLMAP_DIR}" --imagepath "${IMAGE_DIR}" --train_nerf True --outpath "${OUTPUT_DIR}/train.json"

echo 'Train Model'
ns-train nerfacto --data "${OUTPUT_DIR}/train.json" --timestamp "${EXP_NAME}" --output-dir ${OUTPUT_DIR}/out --viewer.quit-on-train-completion True

echo 'Model Evaluation'
ns-eval --load-config "${OUTPUT_DIR}/out/${EXP_NAME}/${MODEL}/${EXP_NAME}/config.yml" --output-path "${OUTPUT_DIR}/out/output.json"